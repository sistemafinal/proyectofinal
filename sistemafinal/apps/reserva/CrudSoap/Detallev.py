
from django.views.decorators.csrf import csrf_exempt
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import Unicode, Integer, Double, String,Decimal
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from spyne.service import ServiceBase
import json
from apps.ventas.models.DetalleVenta import DetalleVenta
from spyne import Iterable,Array
from spyne import ComplexModel
from django.forms.models import model_to_dict
from apps.ventas.always.SearchFilter import keys_add_none
from django.db import IntegrityError
from spyne.error import ResourceNotFoundError
from spyne.model.fault import Fault
from django.db.models.deletion import ProtectedError

class DetelleVens(ComplexModel):
	#__namespace__ = "grupos"
	id = Integer
	cantidad = String 
	precio_venta = Decimal
	descuento = String
	venta_id = Integer
	articulo_id = String

class DetallevenAddEdit(ComplexModel):
	#__namespace__ = "grupos"
	id = Integer
	cantidad = String 
	precio_venta = Decimal
	descuento = String
	venta_id = Integer
	articulo_id = String

class SoapService(ServiceBase):
	@rpc(_returns=Array(DetelleVens))#rpc invocar a una funcion remotamente
	def list(ctx):
		listado = Detalleven.objects.values('id','cantidad','precio_venta',
			'descuento','venta_id','articulo_id')
		return listado

	@rpc(DetallevenAddEdit, _returns=DetelleVens)
	def add(ctx, detalleven):
		data = detalleven.as_dict()
		data_detalleven = keys_add_none(data,'cantidad,precio_venta,'+
			'descuento,venta_id,articulo_id')
		try:
			p = Detalleven.objects.create(**data)
			return Detalleven.objects.filter(id=p.id).values('id','cantidad','precio_venta',
				'descuento','venta_id','articulo_id').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(DetallevenAddEdit, _returns=DetelleVens)
	def update(ctx, detalleven):
		data = detalleven.as_dict()
		data_detalleven = keys_add_none(data,'cantidad,precio_venta,'+
			'descuento,venta_id,articulo_id')
		try:
			p = Detalleven.objects.filter(id=data['id']).update(**data)
			return Detalleven.objects.filter(id=data['id']).values('id','cantidad','precio_venta',
				'descuento','venta_id','articulo_id').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(Integer, _returns=String)
	def delete(ctx, id):
		try:
			p = Detalleven.objects.filter(id=id).delete()
			return "Eliminado "+str(id)
		except ProtectedError as e:
			raise Fault(faultcode="400", faultstring=e.args[0])

my_soap = Application(
	[SoapService],
	tns='django.soap.example',
	in_protocol=Soap11(validator='lxml'),
	out_protocol=Soap11(),
	)

def my_soap_consulta():
	django_app = DjangoApplication(my_soap)
	my_soap_app = csrf_exempt(django_app)
	return my_soap_app