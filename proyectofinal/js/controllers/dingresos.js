angular.module('app').controller('DeingresoCtrl', function (toastr, CONEX, $mdDialog) {

		var self = this;

		self.listar = function(search){
			var filtro = {
				search: search,
			};
			CONEX('ventas','detallein').GET_LIST(filtro).$promise.then(function (r) {
						self.listado = r;
				}, function (err) {
				});
		};

		self.listar();

		self.new_edit = function(ev, obj) {
		$mdDialog.show({
			controller: 'DingresoFormCtrl',
			controllerAs: "form",
			templateUrl: 'views/detallein/form.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:false,
			fullscreen: false, // Only for -xs, -sm breakpoints.
			locals: {obj: obj},
		})
		.then(function(answer) {
			self.listar();
		}, function() {

		});
	};
self.delete = function(ev, obj) {
		// Appending dialog to document.body to cover sidenav in docs app
		var confirm = $mdDialog.confirm()
					.title('¿Desea eliminar el detalle ingreso numero: '+obj['codigo']+'?')
					.textContent('Mira, si eliminas ya no podrás recuperar este detalle ingreso')
					.ariaLabel('a')
					.targetEvent(ev)
					.ok('Si, estoy seguro')
					.cancel('No quiero');

		$mdDialog.show(confirm).then(function() {
			CONEX('ventas','detallein/delete').DELETE({id: obj['id']}).$promise.then(function (r){
					self.listar();
				}, function(err){
					toastr.error(err.data, 'No hemos podido eliminar');
				});
		}, function() {
			toastr.info('Ok, no eliminaste');
		});
	};

});

angular.module('app').controller('DingresoFormCtrl', function (toastr, CONEX, $mdDialog, obj) {
		var self = this;
		self.obj = {};
		self.ingresos = [];
		self.articulos = [];

		if(obj){
			 CONEX('ventas','detallein/edit').GET({id: obj['id']}).$promise.then(function (r){
					self.obj = r;
				}, function(err){

				});
		}

		self.cancel = function(){
				$mdDialog.cancel();
		};

		self.list_tipos = function(search){
				CONEX('ventas','ingresos/searchform').GET_LIST().$promise.then(function (r){
					self.ingresos = r;
				}, function(err){

				});
		};

		self.list_tipos();

		self.list_tipos = function(search){
				CONEX('ventas','articulos/searchform').GET_LIST().$promise.then(function (r){
					self.articulos = r;
				}, function(err){

				});
		};

		self.list_tipos();

		self.guardar = function(search){
			if(self.obj['id']){
			CONEX('ventas','detallein/update').PUT(self.obj).$promise.then(function (r) {
						toastr.success("Detalle ingreso actualizado correctamente");
						$mdDialog.hide(r);
				}, function (err) {
					toastr.error(err.data, "Error al actualizar")
				});
			}else{
			self.obj['estado'] = '1';
			CONEX('ventas','detallein/add').POST(self.obj).$promise.then(function (r) {
						toastr.success("Detalle ingreso creado correctamente");
						$mdDialog.hide(r);
				}, function (err) {
					toastr.error(err.data, "Error al registrar")
				});
		}
		};

});