from django.conf.urls import url, include
from rest_framework import routers
from .apis.TipoUbigeoApi import TipoUbigeoViewSet
from .apis.UbigeoApi import UbigeoViewSet
from .soap import SoapUbigeo


router = routers.DefaultRouter()
router.register(r'ubigeo', UbigeoViewSet)
router.register(r'tipoubigeo', TipoUbigeoViewSet)

urlpatterns = [
	url(r'^', include(router.urls)),
	url(r'^soap/', SoapUbigeo.my_soap_consulta()),
]
