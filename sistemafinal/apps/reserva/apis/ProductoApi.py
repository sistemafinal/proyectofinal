from django.conf.urls import url, include
from ..models.Productos import Productos
from rest_framework import routers, serializers, viewsets
from django.db.models.deletion import ProtectedError
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.reserva.always.SearchFilter import keys_add_none
from rest_framework import permissions 

import requests
from bs4 import BeautifulSoup
import urllib.request
import json
from itertools import chain
from collections import defaultdict


class ProductosSerializer(serializers.ModelSerializer):
	categoria_nombre = serializers.CharField(source='categoria.nombre')
	class Meta:
		model = Productos
		fields = '__all__'

class ProductosInfoSerializer(serializers.ModelSerializer):
	categoria_nombre = serializers.CharField(source='categoria.nombre')
	class Meta:
		model = Productos
		fields = ('id','codigo','nombre','precio_venta',
			'stock','descripcion','estado','categoria_nombre')

class ProductosViewSet(viewsets.ModelViewSet):
	queryset = Productos.objects.all()
	serializer_class = ProductosSerializer
	#permission_classes = [permissions.IsAuthenticated,]

	@list_route(url_path='add', methods=['post'], permission_classes=[])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		columns = 'codigo,nombre,precio_venta,stock,descripcion,estado,categoria_id'
		data_ = keys_add_none(data, columns)
		g = Productos.objects.create(**data_)
		result = self.get_serializer(g).data
		return Response(result)

	@list_route(url_path='edit', methods=['get'], permission_classes=[])
	def get_edit(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		g = Productos.objects.filter(id=id).values().first()
		return Response(g)

	@list_route(url_path='update', methods=['put'], permission_classes=[])
	def get_update(self, request, *args, **kwargs):
		data = request.data
		columns =  'codigo,nombre,precio_venta,stock,descripcion,estado,categoria_id'
		data_ = keys_add_none(data, columns)
		g = Productos.objects.filter(id=data['id']).update(**data_)
		if g:
			model = Productos.objects.get(id=data['id'])
			result = self.get_serializer(model).data
			return Response(result)
		else:
			return Response('No hemos podido actualizar')

	@list_route(url_path='delete', methods=['delete'], permission_classes=[])
	def get_delete(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		try:
			g = Productos.objects.filter(id=id).delete()
			return Response({'id':id, 'msg':'Se eliminó correctamente'})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)


	@list_route(url_path='info', methods=['get'], permission_classes=[])
	def get_info(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		codigo = request.query_params.get('codigo', None)
		filt = {}
		if id:
			filt['id'] = id
		if codigo:
			filt['codigo'] = codigo

		if filt:
			g = Productos.objects.filter(**filt)
			res = ProductosInfoSerializer(g, many=True).data
			if res:
				return Response(res[0])
			else:
				return Response('No hemos encontrado su grupo')
		else:
			return Response('No hemos recibo los parámetros requeridos')


	@list_route(url_path='consultacodigo', methods=['post'], permission_classes=[])
	def get_consultacodigo(self, request, *args, **kwargs):
		data = request.data
		result = {}
		c = Productos.objects.filter(
			codigo=data['codigo']).values('id').first()
		if c:
			result = dict(
				estado=False,
				msg='Este código  ya existe'
				)
		else:
			result = dict(
				estado=True,
				msg='El código  está disponible'
				)
		return Response(result)