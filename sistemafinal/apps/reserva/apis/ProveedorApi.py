from django.conf.urls import url, include
from ..models.Proveedores import Proveedores
from rest_framework import routers, serializers, viewsets
from django.db.models.deletion import ProtectedError
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.reserva.always.SearchFilter import keys_add_none
from rest_framework import permissions 

import requests
from bs4 import BeautifulSoup
import urllib.request
import json
from itertools import chain
from collections import defaultdict


class ProveedoresSerializer(serializers.ModelSerializer):
	persona_nombre = serializers.CharField(source='personas.nombre')
	class Meta:
		model = Proveedores
		fields = '__all__'

class ProveedoresBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = Proveedores
		fields = ('id','contacto','telefono')

class ProveedoresInfoSerializer(serializers.ModelSerializer):
	persona_nombre = serializers.CharField(source='personas.nombre')
	class Meta:
		model = Proveedores
		fields = ('id','contacto','telefono','estado','personas_nombre')

class ProveedoresViewSet(viewsets.ModelViewSet):
	queryset = Proveedores.objects.all()
	serializer_class = ProveedoresSerializer
	#permission_classes = [permissions.IsAuthenticated,]

	@list_route(url_path='searchform', methods=['get'], permission_classes=[])
	def get_searchform(self, request, *args, **kwargs):
		listado = Proveedores.objects.filter().order_by('contacto')
		result = ProveedoresBasicSerializer(listado, many=True).data
		return Response(result)

	@list_route(url_path='add', methods=['post'], permission_classes=[])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		columns = 'contacto,telefono,estado,persona_id'
		data_ = keys_add_none(data, columns)
		g = Proveedores.objects.create(**data_)
		result = self.get_serializer(g).data
		return Response(result)

	@list_route(url_path='edit', methods=['get'], permission_classes=[])
	def get_edit(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		g = Proveedores.objects.filter(id=id).values().first()
		return Response(g)

	@list_route(url_path='update', methods=['put'], permission_classes=[])
	def get_update(self, request, *args, **kwargs):
		data = request.data
		columns =  'contacto,telefono,estado,persona_id'
		data_ = keys_add_none(data, columns)
		g = Proveedores.objects.filter(id=data['id']).update(**data_)
		if g:
			model = Proveedores.objects.get(id=data['id'])
			result = self.get_serializer(model).data
			return Response(result)
		else:
			return Response('No hemos podido actualizar')

	@list_route(url_path='delete', methods=['delete'], permission_classes=[])
	def get_delete(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		try:
			g = Proveedores.objects.filter(id=id).delete()
			return Response({'id':id, 'msg':'Se eliminó correctamente'})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)


	@list_route(url_path='info', methods=['get'], permission_classes=[])
	def get_info(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		contacto = request.query_params.get('contacto', None)
		filt = {}
		if id:
			filt['id'] = id
		if contacto:
			filt['contacto'] = contacto

		if filt:
			g = Proveedores.objects.filter(**filt)
			res = ProveedoresInfoSerializer(g, many=True).data
			if res:
				return Response(res[0])
			else:
				return Response('No hemos encontrado su proveedor')
		else:
			return Response('No hemos recibo los parámetros requeridos')


	@list_route(url_path='consultacontacto', methods=['post'], permission_classes=[])
	def get_consultacontacto(self, request, *args, **kwargs):
		data = request.data
		result = {}
		c = Proveedores.objects.filter(
			contacto=data['contacto']).values('id').first()
		if c:
			result = dict(
				estado=False,
				msg='Este código url ya existe'
				)
		else:
			result = dict(
				estado=True,
				msg='El código url está disponible'
				)
		return Response(result)