from django.db import models

class Categorias(models.Model):
	codigo = models.CharField(max_length=15,unique=True)
	nombre = models.CharField(max_length=140)
	descripcion = models.TextField(blank=True, null=True,)
	estado = models.CharField(max_length=1, default='1')
	
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

class Meta:
	verbose_name = 'Categorias'
	verbose_name_plural = 'Categorias'
	default_permissions = ()


def __str__(self):
	return self.nombre