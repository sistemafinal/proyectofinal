
from django.views.decorators.csrf import csrf_exempt
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import Unicode, Integer, Double, String, DateTime,Date,Decimal
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from spyne.service import ServiceBase
import json
from apps.ventas.models.DetalleIngreso import DetalleIngreso
from spyne import Iterable,Array
from spyne import ComplexModel
from django.forms.models import model_to_dict
from apps.ventas.always.SearchFilter import keys_add_none
from django.db import IntegrityError
from spyne.error import ResourceNotFoundError
from spyne.model.fault import Fault
from django.db.models.deletion import ProtectedError

class Detalles(ComplexModel):
	#__namespace__ = "grupos"
	id = Integer
	cantidad = String 
	precio_compra = Decimal 
	precio_venta = Decimal
	articulo_id = Integer
	ingreso_id = Integer
	codigo = String

class DetalleAddEdit(ComplexModel):
	#__namespace__ = "grupos"
	id = Integer
	cantidad = String 
	precio_compra = Decimal 
	precio_venta = Decimal
	articulo_id = Integer
	ingreso_id = Integer
	codigo = String

		
class SoapService(ServiceBase):

	@rpc(_returns=Array(Detalles))#rpc invocar a una funcion remotamente
	def list(ctx):
		listado = Detalle.objects.values('id','cantidad ','precio_compra','precio_venta',
			'articulo_id','ingreso_id','codigo')
		return listado

	@rpc(DetalleAddEdit, _returns=Detalles)
	def add(ctx, detalle):
		data = detalle.as_dict()
		data_detalle = keys_add_none(data,'cantidad,precio_compra,precio_venta'+
			'articulo_id,ingreso_id,codigo')
		try:
			p = Detalle.objects.create(**data)
			return Detalle.objects.filter(id=p.id).values('id','cantidad ','precio_compra','precio_venta',
				'articulo_id','ingreso_id','codigo').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(DetalleAddEdit, _returns=Detalles)
	def update(ctx, detalle):
		data = detalle.as_dict()
		data_detalle = keys_add_none(data,'cantidad,precio_compra,precio_venta'+
			'articulo_id,ingreso_id,codigo')
		try:
			p = Detalle.objects.filter(id=data['id']).update(**data)
			return Detalle.objects.filter(id=data['id']).values('id','cantidad ','precio_compra','precio_venta',
				'articulo_id','ingreso_id','codigo').first() 
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(Integer, _returns=String)
	def delete(ctx, id):
		try:
			p = Detalle.objects.filter(id=id).delete()
			return "Eliminado "+str(id)
		except ProtectedError as e:
			raise Fault(faultcode="400", faultstring=e.args[0])

my_soap = Application(
	[SoapService],
	tns='django.soap.example',
	in_protocol=Soap11(validator='lxml'),
	out_protocol=Soap11(),
	)

def my_soap_consulta():
	django_app = DjangoApplication(my_soap)
	my_soap_app = csrf_exempt(django_app)
	return my_soap_app