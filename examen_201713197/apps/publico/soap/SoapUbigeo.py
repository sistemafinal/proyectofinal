from django.views.decorators.csrf import csrf_exempt
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import Unicode, Integer, Double, String, DateTime,Date
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from spyne.service import ServiceBase
import json
from apps.publico.models.Ubigeo import Ubigeo
from spyne import Iterable,Array
from spyne import ComplexModel
from django.forms.models import model_to_dict
from apps.publico.always.SearchFilter import keys_add_none
from django.db import IntegrityError
from spyne.error import ResourceNotFoundError
from spyne.model.fault import Fault
from django.db.models.deletion import ProtectedError

class Ubigeos(ComplexModel):
	__namespace__ = "ubigeo"
	id = Integer
	codigo = String 
	nombre = String
	codigo_telefono = String
	capital = String
	padre_id = Integer
	tipo_ubiego_id= Integer

class UbigeoAddEdit(ComplexModel):
	__namespace__ = "ubigeo"
	id = Integer
	codigo = String 
	nombre = String
	codigo_telefono = String
	capital = String
	padre_id = Integer
	tipo_ubiego_id= Integer

		
class SoapService(ServiceBase):

	@rpc(_returns=Array(Ubigeos))#rpc invocar a una funcion remotamente
	def list(ctx):
		listado = Ubigeo.objects.values('id','codigo','nombre',
			'codigo_telefono','capital','padre_id','tipo_ubiego_id')
		return listado

	@rpc(UbigeoAddEdit, _returns=Ubigeos)
	def add(ctx, ubigeo):
		data = ubigeo.as_dict()
		data_ubigeo = keys_add_none(data,'codigo,nombre,'+
			'codigo_telefono,capital,padre_id,tipo_ubiego_id')
		try:
			p = Ubigeo.objects.create(**data)
			return Ubigeo.objects.filter(id=p.id).values('id','codigo','nombre',
			'codigo_telefono','capital','padre_id','tipo_ubiego_id').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(UbigeoAddEdit, _returns=Ubigeos)
	def update(ctx, ubigeo):
		data = ubigeo.as_dict()
		data_ubigeo = keys_add_none(data,'codigo,nombre,'+
			'codigo_telefono,capital,padre_id,tipo_ubiego_id')
		try:
			p = Ubigeo.objects.filter(id=data['id']).update(**data)
			return Ubigeo.objects.filter(id=data['id']).valuesvalues('id','codigo','nombre',
			'codigo_telefono','capital','padre_id','tipo_ubiego_id').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(Integer, _returns=String)
	def delete(ctx, id):
		try:
			p = Ubigeo.objects.filter(id=id).delete()
			return "Eliminado "+str(id)
		except ProtectedError as e:
			raise Fault(faultcode="400", faultstring=e.args[0])

my_soap = Application(
	[SoapService],
	tns='django.soap.example',
	in_protocol=Soap11(validator='lxml'),
	out_protocol=Soap11(),
	)

def my_soap_consulta():
	django_app = DjangoApplication(my_soap)
	my_soap_app = csrf_exempt(django_app)
	return my_soap_app