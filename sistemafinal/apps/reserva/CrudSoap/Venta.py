from django.views.decorators.csrf import csrf_exempt
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import Unicode, Integer, Double, String, DateTime,Date,Decimal
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from spyne.service import ServiceBase
import json
from apps.ventas.models.Venta import Venta
from spyne import Iterable,Array
from spyne import ComplexModel
from django.forms.models import model_to_dict
from apps.ventas.always.SearchFilter import keys_add_none
from django.db import IntegrityError
from spyne.error import ResourceNotFoundError
from spyne.model.fault import Fault
from django.db.models.deletion import ProtectedError

class Ventas(ComplexModel):
	__namespace__ = "Ventas"
	id = Integer
	tipo_comprobante = String 
	codigo= String
	fecha_venta = Date
	impuesto = Decimal
	total_venta = Decimal

class VentaAddEdit(ComplexModel):
	#__namespace__ = "grupos"
	id = Integer
	tipo_comprobante = String 
	codigo= String
	fecha_venta = Date
	impuesto = Decimal
	total_venta = Decimal

class SoapService(ServiceBase):
	@rpc(_returns=Array(Ventas))
	def list(ctx):
		listado = Venta.objects.values('id','codigo','tipo_comprobante',
			'fecha_venta','impuesto','total_venta')
		return listado

	@rpc(VentaAddEdit, _returns=Ventas)
	def add(ctx, venta):
		data = venta.as_dict()
		data_venta = keys_add_none(data,'codigo,tipo_comprobante,'+
			'fecha_venta,impuesto,total_venta')
		try:
			p = Venta.objects.create(**data)
			return Venta.objects.filter(id=p.id).values('id','codigo','tipo_comprobante',
			'fecha_venta','impuesto','total_venta').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(VentaAddEdit, _returns=Ventas)
	def update(ctx, venta):
		data = venta.as_dict()
		data_venta = keys_add_none(data,'codigo,tipo_comprobante,'+
			'fecha_venta,impuesto,total_venta')
		try:
			p = Venta.objects.filter(id=data['id']).update(**data)
			return Venta.objects.filter(id=data['id']).values('id','codigo','tipo_comprobante',
			'fecha_venta','impuesto','total_venta').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(Integer, _returns=String)
	def delete(ctx, id):
		try:
			p = Venta.objects.filter(id=id).delete()
			return "Eliminado "+str(id)
		except ProtectedError as e:
			raise Fault(faultcode="400", faultstring=e.args[0])

	@rpc(Integer(), _returns=Ventas)
	def sumar(ctx, id):
			return Ventas.objects.filter(id=id).values('id','codigo','tipo_comprobante','fecha_venta','impuesto','total_venta').first()

my_soap = Application(
	[SoapService],
	tns='django.soap.example',
	in_protocol=Soap11(validator='lxml'),
	out_protocol=Soap11(),
	)

def my_soap_consulta():
	django_app = DjangoApplication(my_soap)
	my_soap_app = csrf_exempt(django_app)
	return my_soap_app