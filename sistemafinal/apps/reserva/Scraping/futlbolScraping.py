from django.conf.urls import url, include
from rest_framework import routers, serializers, viewsets
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from bs4 import BeautifulSoup
import urllib.request
import requests
import json

class ScrapingViewSet(viewsets.ViewSet):

	@list_route(url_path='football', permission_classes=[], methods=['get'])
	def get_football(self, request, format=None):
		football = request.query_params.get('football',None)
		url_pet = 'https://resultados.as.com/resultados/futbol/primera/clasificacion/'
		
		params = {
			'hTipo': '2',
			'hDni': dni,
			'hApPat': '',
			'hApMat': '',
			'hNombre': ''
		}

		page = requests.post(url_pet, params)
		soup = BeautifulSoup(page.content, 'html.parser')
		listado = soup.find_all("span", {"class": "nombre-equipo"})

		result = []
		for p in listado:
			valor = p.get_text().strip()
			result.append(valor)

		data = {}
		numero = 0
		for p in result:
			numero += 1
			if numero % 2 == 1:
				colum = result[numero-1].replace('Apellidos y Nombres','apellidos_nombres')
				colum = colum.replace('DNI','dni')
				colum = colum.replace('Grupo de Votación','gvotacion')
				colum = colum.replace('Distrito / Ciudad','distrito')
				colum = colum.replace('Provincia / País','provincia')
				colum = colum.replace('Departamento / Continente','departamento')
				data[colum] = result[numero]

		if 'apellidos_nombres' in data:
			ap_nom = data['apellidos_nombres'].split(",")
			data['apellidos'] = ap_nom[0]
			data['nombres'] = ap_nom[1].strip()

		return Response(data)