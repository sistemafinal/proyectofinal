from django.conf.urls import url, include
from ..models.Ubigeo import Ubigeo
from rest_framework import routers, serializers, viewsets
from django.db.models.deletion import ProtectedError
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.publico.always.SearchFilter import keys_add_none
from rest_framework import permissions

import requests
#from bs4 import BeautifulSoup#
import urllib.request
import json
from itertools import chain
from collections import defaultdict

class UbigeoSerializer(serializers.ModelSerializer):
	tipo_ubigeo_nombre = serializers.CharField(source='tipo_ubigeo.nombre')
	padre_nombre = serializers.CharField(source='padre.nombre')
	class Meta:
		model = Ubigeo
		fields = '__all__'

class UbigeoBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ubigeo
        fields = ('codigo','nombre')

class UbigeoInfoSerializer(serializers.ModelSerializer):
	tipo_ubigeo_nombre = serializers.CharField(source='tipo_ubigeo.nombre')
	padre_nombre = serializers.CharField(source='padre.nombre')

	class Meta:
		model = Ubigeo
		fields = ('id','codigo','nombre','codigo_telefono','capital','tipo_ubigeo_nombre','padre_nombre')

class UbigeoViewSet(viewsets.ModelViewSet):
	queryset = Ubigeo.objects.all()
	serializer_class = UbigeoSerializer
	# permissions_classes = [permissions.IsAuthenticated,]

	@list_route(url_path='add', methods=['post'], permission_classes=[])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		columns = 'codigo,nombre,codigo_telefono,capital,tipo_ubigeo_id,padre_id'
		data_ = keys_add_none(data, columns)
		g = Ubigeo.objects.create(**data_)
		result = self.get_serializer(g).data
		return Response(result)

	@list_route(url_path='edit', methods=['get'], permission_classes=[])
	def get_edit(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		g = Ubigeo.objects.filter(id=id).values().first()
		return Response(g)

	@list_route(url_path='update', methods=['put'], permission_classes=[])
	def get_update(self, request, *args, **kwargs):
		data = request.data
		columns = 'codigo,nombre,codigo_telefono,capital,tipo_ubigeo_id, padre_id'
		data_ = keys_add_none(data, columns)
		g = Ubigeo.objects.filter(id=data['id']).update(**data_)
		if g:
			model = Ubigeo.objects.get(id=data['id'])
			result = self.get_serializer(model).data
			return Response(result)
		else:
			return Response('No hemos podido actualizar')

	@list_route(url_path='delete', methods=['delete'], permission_classes=[])
	def get_delete(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		try:
			g = Ubigeo.objects.filter(id=id).delete()
			return Response({'id':id, 'msg':'Se eliminó correctamente'})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)


	@list_route(url_path='consultacodigo', methods=['get'], permission_classes=[])
	def get_consultacodigo(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		codigo= request.query_params.get('codigo', None)
		filt = {}
		if id:
			filt['id'] = id
		if codigo:
			filt['codigo'] = codigo

		if filt:
			g = Ubigeo.objects.filter(**filt)
			res = UbigeoInfoSerializer(g, many=True).data
			if res:
				return Response(res[0])
			else:
				return Response('No hemos encontrado su Ubigeo')
		else:
			return Response('No hemos recibo los parámetros requeridos')

	@list_route(url_path='distrito', methods=['get'], permission_classes=[])
	def get_distrito(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		codigo= request.query_params.get('codigo', None)
		filt = {}
		if id:
			filt['id'] = id
		if codigo:
			filt['codigo'] = codigo

		if filt:
			g = Ubigeo.objects.filter(**filt)
			res = UbigeoInfoSerializer(g, many=True).data
			if res:
				return Response(res[0])
			else:
				return Response('No hemos encontrado su Ubigeo')
		else:
			return Response('No hemos recibo los parámetros requeridos')
