angular.module('app').controller('ProveedorCtrl', function (toastr, CONEX, $mdDialog) {

    var self = this;

    self.listar = function(search){
    	var filtro = {
    		search: search,
    	};
    	CONEX('comun','persona').GET_LIST(filtro).$promise.then(function (r) {
            self.listado = r;
        }, function (err) {
        });
    };

    self.listar();

    self.new_edit = function(ev) {
    $mdDialog.show({
      controller: 'ProveedorFormCtrl',
      controllerAs: "form",
      templateUrl: 'views/persona/form.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:false,
      fullscreen: false // Only for -xs, -sm breakpoints.
    })
    .then(function(answer) {

    }, function() {
        toastr.success("Por sii");
    });
  };

});

angular.module('app').controller('ProveedorCtrl', function (toastr, CONEX, $mdDialog) {
    var self = this;

    self.cancel = function(){
        $mdDialog.cancel();
    };

});