angular.module('app').controller('ClientesCtrl', function (toastr, CONEX, $mdDialog) {

    var self = this;

    self.listar = function(search){
      var filtro = {
        search: search,
      };
      CONEX('reserva','personas').GET_LIST(filtro).$promise.then(function (r) {
            self.listado = r;
        }, function (err) {
        });
    };
    self.listar();

    self.new_edit = function(ev, obj) {
    $mdDialog.show({
      controller: 'ClientesFormCtrl',
      controllerAs: "form",
      templateUrl: 'views/clientes/form.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:false,
      fullscreen: false, // only for -xs, -sm beakpoints
      locals: {obj: obj},
    })
    .then(function(obj) {
      self.listar();
    }, function() {
      
    });
  };

self.delete = function(ev, obj) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('¿Desea eliminar este tipo de persona: '+obj['nombre']+'?')
          .textContent('Mira, si eliminas ya no podrás recuperar este grupo')
          .ariaLabel('a')
          .targetEvent(ev)
          .ok('Si, estoy seguro')
          .cancel('No quiero');

    $mdDialog.show(confirm).then(function() {
      CONEX('reserva','personas/delete').DELETE({id: obj['id']}).$promise.then(function (r){
          self.listar();
        }, function(err){
          toastr.error(err.data, 'No hemos podido eliminar');
        });
    }, function() {
      toastr.info('Ok, no eliminaste');
    });
  };
});

angular.module('app').controller('ClientesFormCtrl', function (toastr, CONEX, $mdDialog, obj) {
    var self = this;
    self.obj = {};
    self.tiposgrupo = [];

    if(obj){
       CONEX('reserva','personas/edit').GET({id: obj['id']}).$promise.then(function (r){
          self.obj = r;
        }, function(err){

        });
    }

    self.cancel = function(){
        $mdDialog.cancel();
    };

    self.guardar = function(search){
      if(self.obj['id']){
      CONEX('reserva','personas/update').PUT(self.obj).$promise.then(function (r) {
            toastr.success("Grupo actualizado correctamente");
            $mdDialog.hide(r);
        }, function (err) {
          toastr.error(err.data, "Error al actualizar")
        });
      }else{
      self.obj['estado'] = '1';
      CONEX('reserva','personas/add').POST(self.obj).$promise.then(function (r) {
            toastr.success("Grupo creado correctamente");
            $mdDialog.hide(r);
        }, function (err) {
          toastr.error(err.data, "Error al registrar")
        });
    }
    };

});