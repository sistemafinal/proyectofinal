from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import url

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^api/reserva/', include('apps.reserva.urls')),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
]