from django.db import models

class Personas(models.Model):
	nombre = models.CharField(max_length=50, unique=True,)
	tipo_doc = models.CharField(max_length=140)
	num_doc = models.CharField(max_length=30, unique=True,)
	direccion = models.CharField(max_length=70, null=True,)
	telefono = models.CharField(max_length=15)
	email = models.CharField(max_length=50)

class Meta:
	verbose_name = 'Persona'
	verbose_name_plural = 'Personas'
	default_permissions = ()

def __str__(self):
	return self.nombre
