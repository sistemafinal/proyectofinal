from django.db import models
from ..models.Categorias import Categorias

class Productos(models.Model):
	categoria = models.ForeignKey(Categorias, related_name='categorias',
								   db_column='categoria_id', on_delete=models.PROTECT)
	codigo = models.CharField(max_length=15, unique=True,)
	nombre = models.CharField(max_length=140)
	precio_venta = models.DecimalField(max_digits=12, decimal_places=2)
	stock = models.IntegerField()
	descripcion = models.TextField(blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
class Meta:
	verbose_name = 'Producto'
	verbose_name_plural = 'Productos'
	default_permissions = ()

def __str__(self):
	return self.nombre
