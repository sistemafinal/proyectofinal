angular.module('app').controller('PorductosCtrl', function (toastr, CONEX, $mdDialog) {

    var self = this;

    self.listar = function(search){
      var filtro = {
        search: search,
      };
      CONEX('ventas','producto').GET_LIST(filtro).$promise.then(function (r) {
            self.listado = r;
        }, function (err) {
        });
    };

    self.listar();

    self.new_edit = function(ev, obj) {
    $mdDialog.show({
      controller: 'ProductosFormCtrl',
      controllerAs: "form",
      templateUrl: 'views/productos/form.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:false,
      fullscreen: false, // only for -xs, -sm beakpoints
      locals: {obj: obj},
    })
    .then(function(obj) {
      self.listar();
    }, function() {
      
    });
  };

self.delete = function(ev, obj) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('¿Desea eliminar el producto: '+obj['nombre']+'?')
          .textContent('Mira, si eliminas ya no podrás recuperar este producto')
          .ariaLabel('a')
          .targetEvent(ev)
          .ok('Si, estoy seguro')
          .cancel('No quiero');

    $mdDialog.show(confirm).then(function() {
      CONEX('ventas','producto/delete').DELETE({id: obj['id']}).$promise.then(function (r){
          self.listar();
        }, function(err){
          toastr.error(err.data, 'No hemos podido eliminar');
        });
    }, function() {
      toastr.info('Ok, no eliminaste');
    });
  };
});

angular.module('app').controller('ProductosFormCtrl', function (toastr, CONEX, $mdDialog, obj) {
    var self = this;
    self.obj = {};
    self.categorias = [];

    if(obj){
       CONEX('ventas','producto/edit').GET({id: obj['id']}).$promise.then(function (r){
          self.obj = r;
        }, function(err){

        });
    }

    self.cancel = function(){
        $mdDialog.cancel();
    };

    self.list_cat = function(search){
        CONEX('ventas','categorias/searchform').GET_LIST().$promise.then(function (r){
          self.categorias = r;
        }, function(err){

        });
    };    

    self.list_cat();

    self.guardar = function(search){
      if(self.obj['id']){
      CONEX('ventas','producto/update').PUT(self.obj).$promise.then(function (r) {
            toastr.success("Articulo actualizado correctamente");
            $mdDialog.hide(r);
        }, function (err) {
          toastr.error(err.data, "Error al actualizar")
        });
      }else{
      self.obj['estado'] = '1';
      CONEX('ventas','producto/add').POST(self.obj).$promise.then(function (r) {
            toastr.success("Articulo creado correctamente");
            $mdDialog.hide(r);
        }, function (err) {
          toastr.error(err.data, "Error al registrar")
        });
    }
    };

});