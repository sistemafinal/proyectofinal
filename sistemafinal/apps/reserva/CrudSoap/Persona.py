
from django.views.decorators.csrf import csrf_exempt
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import Unicode, Integer, Double, String, DateTime,Date
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from spyne.service import ServiceBase
import json
from apps.reserva.models.Personas import Personas
from spyne import Iterable,Array
from spyne import ComplexModel
from django.forms.models import model_to_dict
from apps.reserva.always.SearchFilter import keys_add_none
from django.db import IntegrityError
from spyne.error import ResourceNotFoundError
from spyne.model.fault import Fault
from django.db.models.deletion import ProtectedError

class Personas(ComplexModel):
	__namespace__ = "personas"
	id = Integer
	nombre = String
	tipo_doc = String
	num_doc = String
	telefono = String
	email=String

class PersonaAddEdit(ComplexModel):
	__namespace__ = "personas"
	id = Integer
	nombre = String
	tipo_doc = String
	num_doc = String
	telefono = String
	email=String
class SoapService(ServiceBase):
	@rpc(_returns=Array(Personas))#rpc invocar a una funcion remotamente
	def list(ctx):
		listado = Persona.objects.values('id','nombre',
			'tipo_doc','num_doc','telefono','email')
		return listado

	@rpc(PersonaAddEdit, _returns=Personas)
	def add(ctx,persona):
		data = persona.as_dict()
		data_persona = keys_add_none(data,'nombre,tipo_doc,num_doc,telefono,email')
		try:
			p = Persona.objects.create(**data)
			return Persona.objects.filter(id=p.id).values('id','nombre','tipo_doc','num_doc','telefono','email').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(PersonaAddEdit, _returns=Personas)
	def update(ctx,persona):
		data = persona.as_dict()
		data_persona = keys_add_none(data,'nombre,tipo_doc,num_doc,telefono,email')
		try:
			p = Persona.objects.filter(id=data['id']).update(**data)
			return Persona.objects.filter(id=data['id']).values('id','nombre','tipo_doc','num_doc','telefono','email').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(Integer, _returns=String)
	def delete(ctx, id):
		try:
			p = Persona.objects.filter(id=id).delete()
			return "Eliminado "+str(id)
		except ProtectedError as e:
			raise Fault(faultcode="400", faultstring=e.args[0])

my_soap = Application(
	[SoapService],
	tns='django.soap.example',
	in_protocol=Soap11(validator='lxml'),
	out_protocol=Soap11(),
	)

def my_soap_consulta():
	django_app = DjangoApplication(my_soap)
	my_soap_app = csrf_exempt(django_app)
	return my_soap_app