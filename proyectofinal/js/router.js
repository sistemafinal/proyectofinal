app.config(function ($stateProvider, $locationProvider, $urlRouterProvider, 
    $ocLazyLoadProvider) {
    $locationProvider.hashPrefix('');
    $urlRouterProvider.otherwise("/web");
    $stateProvider
            .state('web', {
                url: '/web',
                data: {page: 'Sistema La Reserva'},
                views: {
                    '': {
                        templateUrl: 'views/base.html',
                        controller:'BaseCtrl',
                    },
                    'menu@web': {
                        templateUrl: 'views/menu.html',
                        controller: 'MenuCtrl'
                    },
                    'barra@web': {
                        templateUrl: 'views/barra.html',
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load('js/controllers/base.js');
                        }]
                }
            })
            
            .state("web.productos", {
                url: "/productos",
                data: {section: 'App', page: 'Productos'},
                templateUrl: "views/productos/index.html",
                controller: 'PorductosCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/productos.js');
                    }]
                }
            })
            .state("web.categorias", {
                url: "/categoria",
                data: {section: 'App', page: 'Categorias'},
                templateUrl: "views/categorias/index.html",
                controller: 'CategoriasCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/categorias.js');
                    }]
                }
            })
            .state("web.ventas", {
                url: "/ventas",
                data: {section: 'App', page: 'Ventas'},
                templateUrl: "views/ventas/index.html",
                controller: 'VentasCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/ventas.js');
                    }]
                }
            })
            .state("web.clientes", {
                url: "/clientes",
                data: {section: 'App', page: 'Clientes'},
                templateUrl: "views/clientes/index.html",
                controller: 'ClientesCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/clientes.js');
                    }]
                }
            })
            .state("web.ingresos", {
                url: "/ingresos",
                data: {section: 'App', page: 'Ingresos'},
                templateUrl: "views/ingresos/index.html",
                controller: 'IngresosCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/ingresos.js');
                    }]
                }
            })
            .state("web.detallev", {
                url: "/detallev",
                data: {section: 'App', page: 'Detallev'},
                templateUrl: "views/detallev/index.html",
                controller: 'DetallevCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/detalle_venta.js');
                    }]
                }
            })
            .state("web.dingresos", {
                url: "/dingreso",
                data: {section: 'App', page: 'Dingresos'},
                templateUrl: "views/detallein/index.html",
                controller: 'DeingresoCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/dingresos.js');
                    }]
                }
            })
            
      $locationProvider.html5Mode(false);
});
