from django.conf.urls import url, include
from ..models.TipoUbigeo import TipoUbigeo
from rest_framework import routers, serializers, viewsets
from django.db.models.deletion import ProtectedError
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.publico.always.SearchFilter import keys_add_none

class TipoUbigeoSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoUbigeo
		fields = '__all__'

class TipoUbigeoBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoUbigeo
		fields = ('id','codigo','nombre','nombre_plural','orden','estado')

class TipoUbigeoViewSet(viewsets.ModelViewSet):
	queryset = TipoUbigeo.objects.all()
	serializer_class = TipoUbigeoSerializer

	@list_route(url_path='searchform', methods=['get'], permission_classes=[])
	def get_searchform(self, request, *args, **kwargs):
		listado = TipoUbigeo.objects.filter().order_by('nombre')
		result = TipoUbigeoBasicSerializer(listado, many=True).data
		return Response(result)

	@list_route(url_path='add', methods=['post'], permission_classes=[])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data_ = keys_add_none(data,'codigo,nombre,nombre_plural,orden,estado')
		e = TipoUbigeo.objects.create(**data_)
		return Response(self.get_serializer(e).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = TipoUbigeo.objects.get(pk=id)
		data = self.get_serializer(model).data
		return Response(data)

	@list_route(url_path='info', methods=['get'], permission_classes=[])
	def get_info(self, request):
		id = request.query_params.get('id', None)
		model = TipoUbigeo.objects.get(pk=id)
		data = self.get_serializer(model).data
		return Response(data)

	@list_route(url_path='update', methods=['put'], permission_classes=[])
	def get_update(self, request, pk=None):
		data = request.data
		data_ = keys_add_none(data,'codigo,nombre,nombre_plural,orden,estado')
		e = TipoUbigeo.objects.filter(id=data['id']).update(**data_)
		model = TipoUbigeo.objects.get(pk=data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = TipoUbigeo.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)
