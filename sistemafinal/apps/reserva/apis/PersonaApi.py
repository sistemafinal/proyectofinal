from django.conf.urls import url, include
from ..models.Personas import Personas
from rest_framework import routers, serializers, viewsets
from django.db.models.deletion import ProtectedError
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.reserva.always.SearchFilter import keys_add_none


class PersonasSerializer(serializers.ModelSerializer):
	class Meta:
		model = Personas
		fields = '__all__'

class PersonasBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Personas
        fields = ('id','nombre','tipo_doc')

class PersonasinfoSerializer(serializers.ModelSerializer):
    persona_nombre = serializers.CharField(source='persona_nombre')

    class Meta:
        model = Personas
        fields = ('id', 'nombre', 'tipo_doc', 'num_doc','direccion','telefono','email',)


class PersonasViewSet(viewsets.ModelViewSet):
    queryset = Personas.objects.all()
    serializer_class = PersonasSerializer

    @list_route(url_path='searchform', methods=['get'], permission_classes=[])
    def get_searchform(self, request, *args, **kwargs):
        listado = Personas.objects.filter().order_by('nombre')
        result = PersonasBasicSerializer(listado, many=True).data
        return Response(result)


    @list_route(url_path='add', methods=['post'], permission_classes=[])
    def get_add(self, request, *args, **kwargs):
        data = request.data
        columns = 'nombre,tipo_doc,num_doc,direccion,telefono,email'
        data_ = keys_add_none(data, columns)
        g = Personas.objects.create(**data_)
        result = self.get_serializer(g).data
        return Response(result)

    @list_route(url_path='edit', methods=['get'], permission_classes=[])
    def get_edit(self, request, *args, **kwargs):
        id = request.query_params.get('id', None)
        g = Personas.objects.filter(id=id).values().first()
        return Response(g)

    @list_route(url_path='update', methods=['put'], permission_classes=[])
    def get_update(self, request, *args, **kwargs):
        data = request.data
        columns = 'nombre,tipo_doc,num_doc,direccion,telefono,email'
        data_ = keys_add_none(data, columns)
        g = Personas.objects.filter(id=data['id']).update(**data)
        if g:
            model = Personas.objects.get(id=data['id'])
            result = self.get_serializer(model).data
            return Response('no hemos podido Actualizar')
        else:
            return Response('NO HEMOS PODIDO ACTUALIZAR')

    @list_route(url_path='delete', methods=['delete'], permission_classes=[])
    def get_delete(self, request, *args, **kwargs):
        id = request.query_params.get('id', None)
        try:
            g = Personas.objects.filter(id=id).delete()
            return Response({'id': id, 'msg': 'se elimino correctamente'})
        except ProtectedError as e:
            return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(url_path='info', methods=['get'], permission_classes=[])
    def get_info(self, request, *args, **kwargs):
        id = request.query_params.get('id', None)
        num_doc = request.query_params.get('codigo', None)
        filt = {}
        if id:
            filt['id'] = id
        if num_doc:
            filt['num_doc'] = num_doc

        if filt:
            g = Personas.objects.filter(**filt)
            res = PersonasinfoSerializer(g, many=True).data
            if res:
                return Response(res[0])
            else:
                return Response('no encontramos su grupo')
        else:
            return Response('No hemos recibido los parametros requeridos')

    @list_route(url_path='consultadoc', methods=['post'], permission_classes=[])
    def get_consultadoc(self, request, *args, **kwargs):
        data = request.data

        c = Personas.objects.filter(
            num_doc=data['num_doc']).values('id').first()
        if c:
           result = dict(
               estado=False,
               msg='Este ya esta disponible'
           )
        else:
            result = dict(
                estado=True,
                msg='el cosigo url esta disponible'
            )
        return Response(result)
