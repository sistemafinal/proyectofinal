from django.db import models
from ..models.Roles import Roles

class Users(models.Model):
	roles = models.ForeignKey(Roles, related_name='roles', db_column='rol_id', on_delete=models.PROTECT)
	usuario = models.CharField(max_length=30, unique=True,)
	password = models.CharField(max_length=140)
	estado = models.CharField(max_length=1, default='1')
	
class Meta:
	verbose_name = 'User'
	verbose_name_plural = 'Users'
	default_permissions = ()

def __str__(self):
	return self.nombre
