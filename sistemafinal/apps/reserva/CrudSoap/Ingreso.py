
from django.views.decorators.csrf import csrf_exempt
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import Unicode, Integer, Double, String, DateTime,Date
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from spyne.service import ServiceBase
import json
from apps.ventas.models.Ingreso import Ingreso
from spyne import Iterable,Array
from spyne import ComplexModel
from django.forms.models import model_to_dict
from apps.ventas.always.SearchFilter import keys_add_none
from django.db import IntegrityError
from spyne.error import ResourceNotFoundError
from spyne.model.fault import Fault
from django.db.models.deletion import ProtectedError

class Ingresos(ComplexModel):
	#__namespace__ = "grupos"
	id= Integer
	proveedor_id = Integer
	tipo_comprobante = String 
	serie_comprobante = String
	num_comprobante = String
	fecha_remision = Date
	impuesto = String
	estado=String

class IngresoAddEdit(ComplexModel):
	#__namespace__ = "grupos"
	id= Integer
	proveedor_id = Integer
	tipo_comprobante = String 
	serie_comprobante = String
	num_comprobante = String
	fecha_remision = Date
	impuesto = String
	estado=String
class SoapService(ServiceBase):
	@rpc(_returns=Array(Ingresos))#rpc invocar a una funcion remotamente
	def list(ctx):
		listado = Ingreso.objects.values('id','proveedor_id','tipo_comprobante',
			'serie_comprobante','num_comprobante','impuesto','estado','fecha_remision')
		return listado

	@rpc(IngresoAddEdit, _returns=Ingresos)
	def add(ctx, ingreso):
		data = ingreso.as_dict()
		data_ingreso = keys_add_none(data,'proveedor_id,tipo_comprobante,'+
			'serie_comprobante,num_comprobante,impuesto,estado,fecha_remision')
		try:
			p = Ingreso.objects.create(**data)
			return Ingreso.objects.filter(id=p.id).values('id','proveedor_id','tipo_comprobante',
				'serie_comprobante','num_comprobante','impuesto',
				'estado','fecha_remision').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(IngresoAddEdit, _returns=Ingresos)
	def update(ctx, ingreso):
		data = ingreso.as_dict()
		data_ingreso = keys_add_none(data,'proveedor_id,tipo_comprobante,'+
			'serie_comprobante,num_comprobante,impuesto,estado,fecha_remision')
		try:
			p = Ingreso.objects.filter(id=data['id']).update(**data)
			return Ingreso.objects.filter(id=data['id']).values('id','proveedor_id','tipo_comprobante',
			'serie_comprobante','num_comprobante',
			'impuesto','estado','fecha_remision').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(Integer, _returns=String)
	def delete(ctx, id):
		try:
			p = Ingreso.objects.filter(id=id).delete()
			return "Eliminado "+str(id)
		except ProtectedError as e:
			raise Fault(faultcode="400", faultstring=e.args[0])

my_soap = Application(
	[SoapService],
	tns='django.soap.example',
	in_protocol=Soap11(validator='lxml'),
	out_protocol=Soap11(),
	)

def my_soap_consulta():
	django_app = DjangoApplication(my_soap)
	my_soap_app = csrf_exempt(django_app)
	return my_soap_app