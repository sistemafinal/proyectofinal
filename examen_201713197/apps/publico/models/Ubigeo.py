from ..models.TipoUbigeo import TipoUbigeo
from django.db import models

class Ubigeo(models.Model):
	codigo = models.CharField(max_length=20, blank=True, null=True)
	nombre = models.CharField(max_length=70)
	tipo_ubigeo = models.ForeignKey(TipoUbigeo, related_name='tipo_ubigeo_ubigeos', 
		db_column='tipo_ubigeo_id', on_delete=models.PROTECT)
	codigo_telefono = models.CharField(max_length=20, blank=True, null=True)
	capital = models.CharField(max_length=80, blank=True, null=True)
	padre = models.ForeignKey('self', related_name='padre_ubigeos', db_column='padre_id', blank=True, null=True, on_delete=models.PROTECT)

	class Meta:
		db_table = 'publico_ubigeo'
		verbose_name = 'Ubigeo'
		verbose_name_plural = 'Ubigeos'
		unique_together = ('nombre','tipo_ubigeo', 'padre')
		default_permissions = ()
		permissions = ()
		
	def __str__(self):
		return  self.nombre