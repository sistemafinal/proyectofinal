-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 21-06-2019 a las 21:14:43
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistemafinal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add categorias', 7, 'add_categorias'),
(20, 'Can change categorias', 7, 'change_categorias'),
(21, 'Can delete categorias', 7, 'delete_categorias'),
(22, 'Can add detalle ingresos', 8, 'add_detalleingresos'),
(23, 'Can change detalle ingresos', 8, 'change_detalleingresos'),
(24, 'Can delete detalle ingresos', 8, 'delete_detalleingresos'),
(25, 'Can add ingresos', 10, 'add_ingresos'),
(26, 'Can change ingresos', 10, 'change_ingresos'),
(27, 'Can delete ingresos', 10, 'delete_ingresos'),
(28, 'Can add personas', 11, 'add_personas'),
(29, 'Can change personas', 11, 'change_personas'),
(30, 'Can delete personas', 11, 'delete_personas'),
(31, 'Can add productos', 12, 'add_productos'),
(32, 'Can change productos', 12, 'change_productos'),
(33, 'Can delete productos', 12, 'delete_productos'),
(34, 'Can add roles', 14, 'add_roles'),
(35, 'Can change roles', 14, 'change_roles'),
(36, 'Can delete roles', 14, 'delete_roles'),
(37, 'Can add users', 15, 'add_users'),
(38, 'Can change users', 15, 'change_users'),
(39, 'Can delete users', 15, 'delete_users'),
(40, 'Can add venta', 16, 'add_venta'),
(41, 'Can change venta', 16, 'change_venta'),
(42, 'Can delete venta', 16, 'delete_venta'),
(43, 'Can add application', 17, 'add_application'),
(44, 'Can change application', 17, 'change_application'),
(45, 'Can delete application', 17, 'delete_application'),
(46, 'Can add access token', 18, 'add_accesstoken'),
(47, 'Can change access token', 18, 'change_accesstoken'),
(48, 'Can delete access token', 18, 'delete_accesstoken'),
(49, 'Can add grant', 19, 'add_grant'),
(50, 'Can change grant', 19, 'change_grant'),
(51, 'Can delete grant', 19, 'delete_grant'),
(52, 'Can add refresh token', 20, 'add_refreshtoken'),
(53, 'Can change refresh token', 20, 'change_refreshtoken'),
(54, 'Can delete refresh token', 20, 'delete_refreshtoken');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(2, 'auth', 'permission'),
(3, 'auth', 'group'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(6, 'sessions', 'session'),
(7, 'reserva', 'categorias'),
(8, 'reserva', 'detalleingresos'),
(9, 'reserva', 'detalleventa'),
(10, 'reserva', 'ingresos'),
(11, 'reserva', 'personas'),
(12, 'reserva', 'productos'),
(13, 'reserva', 'proveedores'),
(14, 'reserva', 'roles'),
(15, 'reserva', 'users'),
(16, 'reserva', 'venta'),
(17, 'oauth2_provider', 'application'),
(18, 'oauth2_provider', 'accesstoken'),
(19, 'oauth2_provider', 'grant'),
(20, 'oauth2_provider', 'refreshtoken');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-06-17 01:13:52.259221'),
(2, 'auth', '0001_initial', '2019-06-17 01:13:53.125884'),
(3, 'admin', '0001_initial', '2019-06-17 01:13:53.261876'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-06-17 01:13:53.277830'),
(5, 'contenttypes', '0002_remove_content_type_name', '2019-06-17 01:13:53.371580'),
(6, 'auth', '0002_alter_permission_name_max_length', '2019-06-17 01:13:53.424437'),
(7, 'auth', '0003_alter_user_email_max_length', '2019-06-17 01:13:53.469318'),
(8, 'auth', '0004_alter_user_username_opts', '2019-06-17 01:13:53.485276'),
(9, 'auth', '0005_alter_user_last_login_null', '2019-06-17 01:13:53.586128'),
(10, 'auth', '0006_require_contenttypes_0002', '2019-06-17 01:13:53.588120'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2019-06-17 01:13:53.602085'),
(12, 'auth', '0008_alter_user_username_max_length', '2019-06-17 01:13:53.643972'),
(13, 'auth', '0009_alter_user_last_name_max_length', '2019-06-17 01:13:53.702378'),
(14, 'oauth2_provider', '0001_initial', '2019-06-17 01:13:54.830492'),
(15, 'oauth2_provider', '0002_08_updates', '2019-06-17 01:13:55.029621'),
(16, 'oauth2_provider', '0003_auto_20160316_1503', '2019-06-17 01:13:55.084717'),
(17, 'oauth2_provider', '0004_auto_20160525_1623', '2019-06-17 01:13:55.531562'),
(18, 'oauth2_provider', '0005_auto_20170514_1141', '2019-06-17 01:13:57.029131'),
(19, 'oauth2_provider', '0006_auto_20171214_2232', '2019-06-17 01:13:57.303536'),
(20, 'reserva', '0001_initial', '2019-06-17 01:13:58.985324'),
(21, 'sessions', '0001_initial', '2019-06-17 01:13:59.076079');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

DROP TABLE IF EXISTS `django_session`;
CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth2_provider_accesstoken`
--

DROP TABLE IF EXISTS `oauth2_provider_accesstoken`;
CREATE TABLE IF NOT EXISTS `oauth2_provider_accesstoken` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `expires` datetime(6) NOT NULL,
  `scope` longtext NOT NULL,
  `application_id` bigint(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `source_refresh_token_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth2_provider_accesstoken_token_8af090f8_uniq` (`token`),
  UNIQUE KEY `source_refresh_token_id` (`source_refresh_token_id`),
  KEY `oauth2_provider_accesstoken_application_id_b22886e1` (`application_id`),
  KEY `oauth2_provider_accesstoken_user_id_6e4c9a65` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth2_provider_application`
--

DROP TABLE IF EXISTS `oauth2_provider_application`;
CREATE TABLE IF NOT EXISTS `oauth2_provider_application` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(100) NOT NULL,
  `redirect_uris` longtext NOT NULL,
  `client_type` varchar(32) NOT NULL,
  `authorization_grant_type` varchar(32) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `skip_authorization` tinyint(1) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`),
  KEY `oauth2_provider_application_client_secret_53133678` (`client_secret`),
  KEY `oauth2_provider_application_user_id_79829054` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth2_provider_grant`
--

DROP TABLE IF EXISTS `oauth2_provider_grant`;
CREATE TABLE IF NOT EXISTS `oauth2_provider_grant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `expires` datetime(6) NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  `scope` longtext NOT NULL,
  `application_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth2_provider_grant_code_49ab4ddf_uniq` (`code`),
  KEY `oauth2_provider_grant_application_id_81923564` (`application_id`),
  KEY `oauth2_provider_grant_user_id_e8f62af8` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth2_provider_refreshtoken`
--

DROP TABLE IF EXISTS `oauth2_provider_refreshtoken`;
CREATE TABLE IF NOT EXISTS `oauth2_provider_refreshtoken` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `access_token_id` bigint(20) DEFAULT NULL,
  `application_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `revoked` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `access_token_id` (`access_token_id`),
  UNIQUE KEY `oauth2_provider_refreshtoken_token_revoked_af8a5134_uniq` (`token`,`revoked`),
  KEY `oauth2_provider_refreshtoken_application_id_2d1c311b` (`application_id`),
  KEY `oauth2_provider_refreshtoken_user_id_da837fce` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_categorias`
--

DROP TABLE IF EXISTS `reserva_categorias`;
CREATE TABLE IF NOT EXISTS `reserva_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(140) NOT NULL,
  `descripcion` longtext,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_categorias`
--

INSERT INTO `reserva_categorias` (`id`, `codigo`, `nombre`, `descripcion`, `estado`, `created_at`, `updated_at`) VALUES
(1, '001', 'tragos regionales', 'tragos pura piña', '1', '2019-06-20 00:00:00.000000', '2019-06-19 00:00:10.000000'),
(2, '1234', 'Cócteles aperitivos', 'hola', '0', '2019-06-19 00:00:00.000257', '2019-06-19 00:00:00.000202'),
(4, '958693485', 'bebidas alcoholicas', NULL, '1', '2019-06-20 04:12:19.543347', '2019-06-20 04:12:19.543347');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_detalleingresos`
--

DROP TABLE IF EXISTS `reserva_detalleingresos`;
CREATE TABLE IF NOT EXISTS `reserva_detalleingresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(12,2) NOT NULL,
  `ingresos_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reserva_detalleingresos_ingresos_id_14552364` (`ingresos_id`),
  KEY `reserva_detalleingresos_producto_id_20910c77` (`producto_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_detalleventa`
--

DROP TABLE IF EXISTS `reserva_detalleventa`;
CREATE TABLE IF NOT EXISTS `reserva_detalleventa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descuento` decimal(5,3) NOT NULL,
  `precio_unit` decimal(5,3) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `venta_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reserva_detalleventa_producto_id_c9dc43fb` (`producto_id`),
  KEY `reserva_detalleventa_venta_id_6fed9680` (`venta_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_ingresos`
--

DROP TABLE IF EXISTS `reserva_ingresos`;
CREATE TABLE IF NOT EXISTS `reserva_ingresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_comprobante` varchar(8) NOT NULL,
  `serie_comprobante` varchar(140) NOT NULL,
  `num_comprobante` varchar(140) NOT NULL,
  `fecha_hora` date DEFAULT NULL,
  `impuesto` decimal(12,2) NOT NULL,
  `total` decimal(12,2) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tipo_comprobante` (`tipo_comprobante`),
  KEY `reserva_ingresos_proveedor_id_9f43d263` (`proveedor_id`),
  KEY `reserva_ingresos_usuario_id_82c42dc2` (`usuario_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_personas`
--

DROP TABLE IF EXISTS `reserva_personas`;
CREATE TABLE IF NOT EXISTS `reserva_personas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(8) NOT NULL,
  `tipo_doc` varchar(140) NOT NULL,
  `num_doc` varchar(10) NOT NULL,
  `direccion` varchar(70) DEFAULT NULL,
  `telefono` varchar(140) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  UNIQUE KEY `num_doc` (`num_doc`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_personas`
--

INSERT INTO `reserva_personas` (`id`, `nombre`, `tipo_doc`, `num_doc`, `direccion`, `telefono`, `email`) VALUES
(1, 'Fermin', 'PASAPORTE', '2095952344', 'LAMAS', '951473786', 'oswaldochuquipoma@upeu.edu.pe'),
(2, 'Andy', 'DNI', '05697060', 'Morales', '1222334444', 'wwwww');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_productos`
--

DROP TABLE IF EXISTS `reserva_productos`;
CREATE TABLE IF NOT EXISTS `reserva_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(8) NOT NULL,
  `nombre` varchar(140) NOT NULL,
  `precio_venta` decimal(12,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `descripcion` longtext,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `reserva_productos_categorias_id_15bebf86` (`categoria_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_productos`
--

INSERT INTO `reserva_productos` (`id`, `codigo`, `nombre`, `precio_venta`, `stock`, `descripcion`, `estado`, `created_at`, `updated_at`, `categoria_id`) VALUES
(1, '876', 'Coctel de cacadossss', '10.45', 345869, 'tomas pero no emborracha', '1', '2019-06-19 00:00:00.101000', '2019-06-19 00:00:00.187000', 1),
(2, '2017131', 'ROMPE CALSON', '8.60', 128, 'DE LOS QE TOMA MI AMIGO VELITA PARA APROBAR EL EXAMEN DE METODOS', '1', '2019-06-20 17:31:23.681201', '2019-06-20 17:31:23.681201', 1),
(3, '34', 'Achuni ullo', '12.40', 12, NULL, '1', '2019-06-20 21:52:18.793535', '2019-06-20 21:52:18.794528', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_proveedores`
--

DROP TABLE IF EXISTS `reserva_proveedores`;
CREATE TABLE IF NOT EXISTS `reserva_proveedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contacto` varchar(40) NOT NULL,
  `telefono` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `persona_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contacto` (`contacto`),
  KEY `reserva_proveedores_persona_id_e04194b3` (`persona_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_roles`
--

DROP TABLE IF EXISTS `reserva_roles`;
CREATE TABLE IF NOT EXISTS `reserva_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(140) NOT NULL,
  `descripcion` longtext,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_roles`
--

INSERT INTO `reserva_roles` (`id`, `nombre`, `descripcion`, `estado`) VALUES
(1, 'Administrador', 'El que administra las ares', '1'),
(2, 'Vendedor', 'El encargado de vender', '1'),
(3, 'Almacenero', 'EL ENCARGADO DEL AREA DE LOGISTICA', '1'),
(4, 'Cobrador', 'El que recibe la plata', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_users`
--

DROP TABLE IF EXISTS `reserva_users`;
CREATE TABLE IF NOT EXISTS `reserva_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(8) NOT NULL,
  `password` varchar(140) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `rol_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario` (`usuario`),
  KEY `reserva_users_rol_id_d6e682fd` (`rol_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_venta`
--

DROP TABLE IF EXISTS `reserva_venta`;
CREATE TABLE IF NOT EXISTS `reserva_venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(8) NOT NULL,
  `serie_comprobante` varchar(50) NOT NULL,
  `num_comprobante` varchar(50) NOT NULL,
  `fecha_hora` date NOT NULL,
  `impuesto` decimal(5,3) NOT NULL,
  `total` decimal(5,3) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `reserva_venta_cliente_id_047196af` (`cliente_id`),
  KEY `reserva_venta_user_id_4f9e5d11` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
