angular.module('app').controller('BaseCtrl', function ($scope,
        $state, $mdSidenav, $timeout, $rootScope, data_app) {

    $scope.$on('ocLazyLoad.moduleLoaded', function (e, params) {
    });

    $scope.$on('ocLazyLoad.componentLoaded', function (e, params) {
    });

    $scope.$on('ocLazyLoad.fileLoaded', function (e, file) {
    });
    $scope.app = data_app;
    $rootScope.state = $state;
    $rootScope.Indeterminado = true;

    function openPage() {
        $scope.closeAside();
    }

    $scope.goBack = function () {
        $window.history.back();
    };

    $scope.openAside = function () {
        $timeout(function () {
            $mdSidenav('menu').open();
        });
    };
    $scope.closeAside = function () {
        $timeout(function () {
            $document.find('#menu').length && $mdSidenav('menu').close();
        });
    };

    $scope.menu_folder = function () {
        if ($scope.app.setting.menuFolded) {
            $scope.app.setting.menuFolded = false;
        } else {
            $scope.app.setting.menuFolded = true;
        }
    };

    $scope.aside_folder = function () {
        if ($scope.app.setting.asideFolded) {
            $scope.app.setting.asideFolded = false;
        } else {
            $scope.app.setting.asideFolded = true;
        }
    };

    $scope.AbrirToogle = correrToogle('sid_config_app');

    function correrToogle(navID) {
        return function () {
            $mdSidenav(navID)
            .toggle()
            .then(function () {
            });
        };
    }
    $scope.close_sidenav = function (idNav) {
        $mdSidenav(idNav).close()
        .then(function () {
        });
    };

});

angular.module('app').controller('MenuCtrl', function ($scope, $mdSidenav, data_app) {

    $scope.menu = [
                    {
                        "state":"web.1",
                        "title":"Almacen","type":"link"
                    },
                     {
                        "children":[
                                {
                                    "icon":"person",
                                    "state":"web.categorias",
                                    "title":"Categorias",
                                    "type":"link"
                                }
                            ]
                    },
                    {
                        "children":[
                                {
                                    "icon":"person",
                                    "state":"web.productos",
                                    "title":"Producto",
                                    "type":"link"
                                }
                            ]
                    },
                    {
                        "state":"web.2",
                        "title":"Compras","type":"link"
                    },
                   
                     {

                        "state":"web.2",
                        "title":"Ventas de produtos","type":"link"
                    },
                     {
                        "children":[
                                {
                                    "icon":"person",
                                    "state":"web.clientes",
                                    "title":"Clientes",
                                    "type":"link"
                                }
                            ]
                    },
                    {
                        "state":"web.2",
                        "title":"users","type":"link"
                    },
                    {
                       
                        "state":"web.2",
                        "title":" ","type":"link"
                    },
                    ];
    
    $scope.app = data_app;
    $scope.close_sidenav = function (idNav) {
        $mdSidenav(idNav).close()
        .then(function () {
        });
    };

});
