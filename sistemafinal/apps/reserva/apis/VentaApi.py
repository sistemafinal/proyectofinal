from django.conf.urls import url, include
from ..models.Venta import Venta
from rest_framework import routers, serializers, viewsets
from django.db.models.deletion import ProtectedError
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.reserva.always.SearchFilter import keys_add_none
from rest_framework import permissions
import requests
from bs4 import BeautifulSoup
import urllib.request
import json
from itertools import chain
from collections import defaultdict

class VentaSerializer(serializers.ModelSerializer):
	persona_nombre = serializers.CharField(source='cliente.nombre')
	users_nombre = serializers.CharField(source='user.usuario')
	class Meta:
		model = Venta
		fields = '__all__'

class VentaBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = Venta
		fields = ('id', 'codigo', 'total')


class VentainfoSerializer(serializers.ModelSerializer):
	personas_nombre = serializers.CharField(source='personas.nombre')
	users_nombre = serializers.CharField(source='users.usuario')
	class Meta:      
		model = Venta
		fields = ('id', 'codigo', 'serie_comprobante', 'num_comprobante', 'fecha_hora', 'impuesto', 'total', 'cliente_nombre', 'users_nombre',)


class VentaViewSet(viewsets.ModelViewSet):
	queryset = Venta.objects.all()
	serializer_class = VentaSerializer

	@list_route(url_path='searchform', methods=['get'], permission_classes=[])
	def get_searchform(self, request, *args, **kwargs):
		listado = Venta.objects.filter().order_by('codigo')
		result = VentaBasicSerializer(listado, many=True).data
		return Response(result)

	@list_route(url_path='add', methods=['post'], permission_classes=[])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data_ = keys_add_none(data,'id,codigo,serie_comprobante,num_comprobante,fecha_hora,impuesto,total,cliente_id,user_id')
		e = Venta.objects.create(**data_)
		return Response(self.get_serializer(e).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[])
	def get_edit(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		g = Venta.objects.filter(id=id).values().first()
		return Response(g)

	@list_route(url_path='update', methods=['put'], permission_classes=[])
	def get_update(self, request, *args, **kwargs):
		data = request.data
		columns = 'codigo,serie_comprobante,num_comprobante,fecha_hora,impuesto,total,cliente_id,user_id'
		data_ = keys_add_none(data, columns)
		g = Venta.objects.filter(id=data['id']).update(**data)
		if g:
			model = Venta.objects.get(id=data['id'])
			result = self.get_serializer(model).data
			return Response('no hemos podido Actualizar')
		else:
			return Response('NO HEMOS PODIDO ACTUALIZAR')

	@list_route(url_path='delete', methods=['delete'], permission_classes=[])
	def get_delete(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		try:
			g = Venta.objects.filter(id=id).delete()
			return Response({'id': id, 'msg': 'se elimino correctamente'})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='info', methods=['get'], permission_classes=[])
	def get_info(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		codigo = request.query_params.get('codigo', None)
		filt = {}
		if id:
			filt['id'] = id
		if codigo:
			filt['codigo'] = codigo

		if filt:
			g = Venta.objects.filter(**filt)
			res = VentaInfoSerializer(g, many=True).data
			if res:
				return Response(res[0])
			else:
				return Response('No hemos encontrado su venta')
		else:
			return Response('No hemos recibo los parámetros requeridos')

	@list_route(url_path='consultacodigo', methods=['post'], permission_classes=[])
	def get_consultacodigo(self, request, *args, **kwargs):
		data = request.data
		result = {}
		c = Venta.objects.filter(codigo=data['codigo']).values('id').first()
		if c:
		   result = dict(
			   estado=False,
			   msg='Este CODIGO ya esta disponible'
		   )
		else:
			result = dict(
				estado=True,
				msg='el codigo esta disponible'
			)
		return Response(result)

	