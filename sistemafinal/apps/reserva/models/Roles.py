from django.db import models

class Roles(models.Model):
	nombre = models.CharField(max_length=140)
	descripcion = models.TextField(blank=True, null=True,)
	estado = models.CharField(max_length=1, default='1')

class Meta:
	verbose_name = 'Rol'
	verbose_name_plural = 'Roles'
	default_permissions = ()


def __str__(self):
	return self.nombre