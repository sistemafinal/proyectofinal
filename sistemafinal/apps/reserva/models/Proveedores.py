from django.db import models
from ..models.Personas import Personas

class Proveedores(models.Model):
	personas = models.ForeignKey(Personas, related_name='persona_proveedor', 
		db_column='persona_id', on_delete=models.PROTECT, blank=True, null=True,)
	contacto = models.CharField(max_length=40,unique=True)
	telefono = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')
	
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'reserva_proveedores'
		verbose_name = 'Proveedor'
		verbose_name_plural = 'Proveedores'
		default_permissions = ()

	def __str__(self):
		return self.estado
		