angular.module('app').controller('DetallevCtrl', function (toastr, CONEX, $mdDialog) {

    var self = this;

    self.listar = function(search){
      var filtro = {
        search: search,
      };
      CONEX('ventas','detallev').GET_LIST(filtro).$promise.then(function (r) {
            self.listado = r;
        }, function (err) {
        });
    };

    self.listar();

    self.new_edit = function(ev, obj) {
    $mdDialog.show({
      controller: 'DetallevFormCtrl',
      controllerAs: "form",
      templateUrl: 'views/detallev/form.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:false,
      fullscreen: false, // Only for -xs, -sm breakpoints.
     locals: {obj: obj},
    })
    .then(function(answer) {
      self.listar();
    }, function() {

    });
  };
self.delete = function(ev, obj) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('¿Desea eliminar este detalle venta: ')
          .textContent('Mira, si eliminas ya no podrás recuperar este detalle venta')
          .ariaLabel('a')
          .targetEvent(ev)
          .ok('Si, estoy seguro')
          .cancel('No quiero');

    $mdDialog.show(confirm).then(function() {
      CONEX('ventas','detallev/delete').DELETE({id: obj['id']}).$promise.then(function (r){
          self.listar();
        }, function(err){
          toastr.error(err.data, 'No hemos podido eliminar');
        });
    }, function() {
      toastr.info('Ok, no eliminaste');
    });
  };

});

angular.module('app').controller('DetallevFormCtrl', function (toastr, CONEX, $mdDialog, obj) {
    var self = this;
    self.obj = {};
    self.ventas = [];
    self.articulos = [];

    if(obj){
       CONEX('ventas','detallev/edit').GET({id: obj['id']}).$promise.then(function (r){
          self.obj = r;
        }, function(err){

        });
    }

    self.cancel = function(){
        $mdDialog.cancel();
    };

    self.list_tipos = function(search){
        CONEX('ventas','ventas/searchform').GET_LIST().$promise.then(function (r){
          self.ventas = r;
        }, function(err){

        });
    };

    self.list_tipos();

        self.list_tipos = function(search){
        CONEX('ventas','articulos/searchform').GET_LIST().$promise.then(function (r){
          self.articulos = r;
        }, function(err){

        });
    };

    self.list_tipos();

    self.guardar = function(search){
      if(self.obj['id']){
      CONEX('ventas','detallev/update').PUT(self.obj).$promise.then(function (r) {
            toastr.success("Detalle venta actualizado correctamente");
            $mdDialog.hide(r);
        }, function (err) {
          toastr.error(err.data, "Error al actualizar")
        });
      }else{
      self.obj['estado'] = '1';
      CONEX('ventas','detallev/add').POST(self.obj).$promise.then(function (r) {
            toastr.success("Detalle venta creado correctamente");
            $mdDialog.hide(r);
        }, function (err) {
          toastr.error(err.data, "Error al registrar")
        });
    }
    };

});