from django.conf.urls import url, include
from ..models.DetalleVenta import DetalleVenta
from rest_framework import routers, serializers, viewsets
from django.db.models.deletion import ProtectedError
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.reserva.always.SearchFilter import keys_add_none
from rest_framework import permissions 

import requests
from bs4 import BeautifulSoup
import urllib.request
import json
from itertools import chain
from collections import defaultdict

class DetalleVentaSerializer(serializers.ModelSerializer):
	productos_nombre = serializers.CharField(source='productos.nombre')
	venta_codigo = serializers.CharField(source='venta.codigo')
	class Meta:
		model = DetalleVenta
		fields = '__all__'

class DetalleVentainfoSerializer(serializers.ModelSerializer):
	productos_nombre = serializers.CharField(source='productos.nombre')
	venta_codigo = serializers.CharField(source='venta.codigo')
	class Meta:
		model = DetalleVenta
		fields = ('id', 'descuento', 'precio_unit', 'cantidad', 'productos_nombre', 'venta_codigo',)

class DetalleVentaViewSet(viewsets.ModelViewSet):
    queryset = DetalleVenta.objects.all()
    serializer_class = DetalleVentaSerializer

    @list_route(url_path='add', methods=['post'], permission_classes=[])
    def get_add(self, request, *args, **kwargs):
        data = request.data
        columns = 'descuento,precio_unit,cantidad,producto_id,venta_id'
        data_ = keys_add_none(data, columns)
        g = DetalleVenta.objects.create(**data_)
        result = self.get_serializer(g).data
        return Response(result)

    @list_route(url_path='edit', methods=['get'], permission_classes=[])
    def get_edit(self, request, *args, **kwargs):
        id = request.query_params.get('id', None)
        g = DetalleVenta.objects.filter(id=id).values().first()
        return Response(g)

    @list_route(url_path='update', methods=['put'], permission_classes=[])
    def get_update(self, request, *args, **kwargs):
        data = request.data
        columns = 'descuento,precio_unit,cantidad,producto_id,venta_id'
        data_ = keys_add_none(data, columns)
        g = DetalleVenta.objects.filter(id=data['id']).update(**data)
        if g:
            model = DetalleVenta.objects.get(id=data['id'])
            result = self.get_serializer(model).data
            return Response('no hemos podido Actualizar')
        else:
            return Response('NO HEMOS PODIDO ACTUALIZAR')

    @list_route(url_path='delete', methods=['delete'], permission_classes=[])
    def get_delete(self, request, *args, **kwargs):
        id = request.query_params.get('id', None)
        try:
            g = DetalleVenta.objects.filter(id=id).delete()
            return Response({'id': id, 'msg': 'se elimino correctamente'})
        except ProtectedError as e:
            return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(url_path='info', methods=['get'], permission_classes=[])
    def get_info(self, request, *args, **kwargs):
        id = request.query_params.get('id', None)
        codigo = request.query_params.get('codigo', None)
        filt = {}
        if id:
            filt['id'] = id
        if codigo:
            filt['codigo'] = codigo
        if filt:
            g = DetalleVenta.objects.filter(**filt)
            res = DetalleVentainfoSerializer(g, many=True).data
            if res:
                return Response(res[0])
            else:
                return Response('no encontramos su detalle de venta')
        else:
            return Response('No hemos recibido los parametros requeridos')

    @list_route(url_path='consultacodigo', methods=['post'], permission_classes=[])
    def get_consultacodigo(self, request, *args, **kwargs):
        data = request.data
        result = {}
        c = DetalleVenta.objects.filter(codigo=data['codigo']).values('id').first()
        if c:
           result = dict(
               estado=False,
               msg='Este CODIGO ya esta disponible'
           )
        else:
            result = dict(
                estado=True,
                msg='el codigo esta disponible'
            )
        return Response(result)