from django.conf.urls import url, include
from ..models.Roles import Roles
from rest_framework import routers, serializers, viewsets
from django.db.models.deletion import ProtectedError
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.reserva.always.SearchFilter import keys_add_none

class RolesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Roles
		fields = '__all__'

class RolesBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = Roles
		fields = ('id','nombre','estado')

class RolesViewSet(viewsets.ModelViewSet):
	queryset = Roles.objects.all()
	serializer_class = RolesSerializer

	@list_route(url_path='searchform', methods=['get'], permission_classes=[])
	def get_searchform(self, request, *args, **kwargs):
		listado = Roles.objects.filter().order_by('nombre')
		result = RolesBasicSerializer(listado, many=True).data
		return Response(result)

	@list_route(url_path='add', methods=['post'], permission_classes=[])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data_ = keys_add_none(data,'nombre,descripcion,estado')
		e = Roles.objects.create(**data_)
		return Response(self.get_serializer(e).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Roles.objects.get(pk=id)
		data = self.get_serializer(model).data
		return Response(data)

	@list_route(url_path='info', methods=['get'], permission_classes=[])
	def get_info(self, request):
		id = request.query_params.get('id', None)
		model = Roles.objects.get(pk=id)
		data = self.get_serializer(model).data
		return Response(data)

	@list_route(url_path='update', methods=['put'], permission_classes=[])
	def get_update(self, request, pk=None):
		data = request.data
		data_ = keys_add_none(data,'nombre,nombre_plural,abreviatura,codigo')
		e = Roles.objects.filter(id=data['id']).update(**data_)
		model = Roles.objects.get(pk=data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Roles.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

