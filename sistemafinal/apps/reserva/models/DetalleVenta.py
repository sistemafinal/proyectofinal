from django.db import models
from ..models.Productos import Productos
from ..models.Venta import Venta

class DetalleVenta(models.Model):
	producto = models.ForeignKey(Productos, related_name='producto_detalleventa', 
		db_column='producto_id', on_delete=models.PROTECT, blank=True, null=True,)
	venta = models.ForeignKey(Venta, related_name='venta_detalleventa', 
		db_column='venta_id', on_delete=models.PROTECT, blank=True, null=True,)
	descuento = models.DecimalField(max_digits=12, decimal_places=2,)
	precio_unit = models.DecimalField(max_digits=12, decimal_places=2,)
	cantidad = models.IntegerField(default=0)
	
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'DetalleVenta'
		verbose_name_plural = 'DetalleVentas'
		default_permissions = ()

	def __str__(self):
		return self.nombre