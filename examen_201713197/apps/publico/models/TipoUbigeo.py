from django.db import models
class TipoUbigeo(models.Model):
	codigo = models.CharField(max_length=8, unique=True)
	nombre = models.CharField(max_length=50, unique=True)
	nombre_plural = models.CharField(max_length=60)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'publico_tipo_ubigeo'
		verbose_name = 'Tipo Ubigeo'
		verbose_name_plural = 'Tipos de Ubigeos'
		default_permissions = ()
		permissions = ()

	def __str__(self):
		return  self.nombre