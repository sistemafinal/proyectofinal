
from django.views.decorators.csrf import csrf_exempt
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import Unicode, Integer, Double, String, DateTime,Date
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from spyne.service import ServiceBase
import json
from apps.ventas.models.Categoria import Categoria
from spyne import Iterable,Array
from spyne import ComplexModel
from django.forms.models import model_to_dict
from apps.ventas.always.SearchFilter import keys_add_none
from django.db import IntegrityError
from spyne.error import ResourceNotFoundError
from spyne.model.fault import Fault
from django.db.models.deletion import ProtectedError

class Categorias(ComplexModel):
	#__namespace__ = "grupos"
	id = Integer
	codigo =  String
	nombre = String
	descripcion= String
	estado = String

class CategoriaAddEdit(ComplexModel):
	id = Integer
	codigo =  String
	nombre = String
	descripcion= String
	estado = String		
class SoapService(ServiceBase):
	@rpc(_returns=Array(Categorias))#rpc invocar a una funcion remotamente
	def list(ctx):
		listado = Categoria.objects.values('id','codigo','nombre','descripcion','estado')
		return listado

	@rpc(CategoriaAddEdit, _returns=Categorias)
	def add(ctx, categoria):
		data = categoria.as_dict()
		data_categoria = keys_add_none(data,'codigo,nombre,descripcion'+'estado')
		try:
			p = Categoria.objects.create(**data)
			return Categoria.objects.filter(id=p.id).values('id','codigo','nombre','descripcion','estado').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(CategoriaAddEdit, _returns=Categorias)
	def update(ctx, categoria):
		data = categoria.as_dict()
		data_categoria = keys_add_none(data,'codigo,nombre,descripcion'+'estado')
		try:
			p = Categoria.objects.filter(id=data['id']).update(**data)
			return Categoria.objects.filter(id=data['id']).values('id','codigo','nombre','descripcion','estado').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(Integer, _returns=String)
	def delete(ctx, id):
		try:
			p = Categoria.objects.filter(id=id).delete()
			return "Eliminado "+str(id)
		except ProtectedError as e:
			raise Fault(faultcode="400", faultstring=e.args[0]) 

my_soap = Application(
	[SoapService],
	tns='django.soap.example',
	in_protocol=Soap11(validator='lxml'),
	out_protocol=Soap11(),
	)

def my_soap_consulta():
	django_app = DjangoApplication(my_soap)
	my_soap_app = csrf_exempt(django_app)
	return my_soap_app 
	