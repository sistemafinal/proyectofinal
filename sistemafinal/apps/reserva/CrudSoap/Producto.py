from django.views.decorators.csrf import csrf_exempt
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import Unicode, Integer, Double, String, DateTime,Date,Decimal
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from spyne.service import ServiceBase
import json
from apps.reserva.models.Productos import Productos
from spyne import Iterable,Array
from spyne import ComplexModel
from django.forms.models import model_to_dict
from apps.reserva.always.SearchFilter import keys_add_none
from django.db import IntegrityError
from spyne.error import ResourceNotFoundError
from spyne.model.fault import Fault
from django.db.models.deletion import ProtectedError

class Productos(ComplexModel):
	__namespace__ = "Ventas"
	id = Integer
	codigo =  String
	nombre = String
	precio_venta = Decimal
	stock = Integer
	descripcion= String
	estado = String
	categoria_id = Integer

class ProductoAddEdit(ComplexModel):
	#__namespace__ = "grupos"
	id = Integer
	codigo =  String
	nombre = String
	precio_venta = Decimal
	stock = Integer
	descripcion= String
	estado = String
	categoria_id = Integer

class SoapService(ServiceBase):
	@rpc(_returns=Array(Productos))
	def list(ctx):
		listado = Producto.objects.values('id','codigo','nombre',
			'precio_venta ','stock ','descripcion','estado','categoria_id ')
		return listado

	@rpc(ProductoAddEdit, _returns=Productos)
	def add(ctx, producto):
		data = producto.as_dict()
		data_producto = keys_add_none(data,'codigo,nombre, precio_venta'+
			'stock,descripcion,estado,categoria_id')
		try:
			p = producto.objects.create(**data)
			return producto.objects.filter(id=p.id).values('id','codigo','nombre',
			'precio_venta ','stock ','descripcion','estado','categoria_id ').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(ProductoAddEdit, _returns=Productos)
	def update(ctx, producto):
		data = venta.as_dict()
		data_venta = keys_add_none(data,'codigo,tipo_comprobante,'+
			'fecha_venta,impuesto,total_venta')
		try:
			p = Producto.objects.filter(id=data['id']).update(**data)
			return Producto.objects.filter(id=data['id']).values('id','codigo','nombre',
			'precio_venta ','stock ','descripcion','estado','categoria_id ').first()
		except IntegrityError as e:
			raise Fault(faultcode=str(e.args[0]), faultstring=e.args[1])

	@rpc(Integer, _returns=String)
	def delete(ctx, id):
		try:
			p = Producto.objects.filter(id=id).delete()
			return "Eliminado "+str(id)
		except ProtectedError as e:
			raise Fault(faultcode="400", faultstring=e.args[0])

my_soap = Application(
	[SoapService],
	tns='django.soap.example',
	in_protocol=Soap11(validator='lxml'),
	out_protocol=Soap11(),
	)

def my_soap_consulta():
	django_app = DjangoApplication(my_soap)
	my_soap_app = csrf_exempt(django_app)
	return my_soap_app