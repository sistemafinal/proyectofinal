angular.module('app').controller('IngresosCtrl', function (toastr, CONEX, $mdDialog) {

    var self = this;

    self.listar = function(search){
      var filtro = {
        search: search,
      };
      CONEX('ventas','ingresos').GET_LIST(filtro).$promise.then(function (r) {
            self.listado = r;
        }, function (err) {
        });
    };

    self.listar();

    self.new_edit = function(ev, obj) {
    $mdDialog.show({
      controller: 'IngresosFormCtrl',
      controllerAs: "form",
      templateUrl: 'views/ingresos/form.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:false,
      fullscreen: false, // only for -xs, -sm beakpoints
      locals: {obj: obj},
    })
    .then(function(obj) {
      self.listar();
    }, function() {
      
    });
  };

self.delete = function(ev, obj) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('¿Desea eliminar el grupo: '+obj['nombre']+'?')
          .textContent('Mira, si eliminas ya no podrás recuperar este grupo')
          .ariaLabel('a')
          .targetEvent(ev)
          .ok('Si, estoy seguro')
          .cancel('No quiero');

    $mdDialog.show(confirm).then(function() {
      CONEX('ventas','ingresos/delete').DELETE({id: obj['id']}).$promise.then(function (r){
          self.listar();
        }, function(err){
          toastr.error(err.data, 'No hemos podido eliminar');
        });
    }, function() {
      toastr.info('Ok, no eliminaste');
    });
  };
});

angular.module('app').controller('IngresosFormCtrl', function (toastr, CONEX, $mdDialog, obj) {
    var self = this;
    self.obj = {};
    self.tipospersonas = [];

    if(obj){
       CONEX('ventas','ingresos/edit').GET({id: obj['id']}).$promise.then(function (r){
          self.obj = r;
        }, function(err){

        });
    }

    self.cancel = function(){
        $mdDialog.cancel();
    };

    self.list_tipos = function(search){
        CONEX('ventas','tipospersonas/searchform').GET_LIST().$promise.then(function (r){
          self.tipospersonas = r;
        }, function(err){

        });
    };    

    self.list_tipos();

    self.guardar = function(search){
      if(self.obj['id']){
      CONEX('ventas','ingresos/update').PUT(self.obj).$promise.then(function (r) {
            toastr.success("Grupo actualizado correctamente");
            $mdDialog.hide(r);
        }, function (err) {
          toastr.error(err.data, "Error al actualizar")
        });
      }else{
      self.obj['estado'] = '1';
      CONEX('ventas','ingresos/add').POST(self.obj).$promise.then(function (r) {
            toastr.success("Grupo creado correctamente");
            $mdDialog.hide(r);
        }, function (err) {
          toastr.error(err.data, "Error al registrar")
        });
    }
    };

});