from django.db import models
from ..models.Proveedores import Proveedores
from ..models.Users import Users

class Ingresos(models.Model):
	proveedores = models.ForeignKey(Proveedores, related_name='proveedores',
								  db_column='proveedor_id', on_delete=models.PROTECT)
	usuario = models.ForeignKey(Users, related_name='users',
								  db_column='usuario_id', on_delete=models.PROTECT)
	tipo_comprobante = models.CharField(max_length=20, unique=True,)
	serie_comprobante = models.CharField(max_length=140)
	num_comprobante = models.CharField(max_length=140)
	fecha_hora = models.DateField(blank=True, null=True,)
	impuesto = models.DecimalField(max_digits=12, decimal_places=2)
	total = models.DecimalField(max_digits=12, decimal_places=2)
	estado = models.CharField(max_length=1, default='1')

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


class Meta:
	verbose_name = 'Ingreso'
	verbose_name_plural = 'Ingreso'
	default_permissions = ()


def __str__(self):
	return self.nombre
