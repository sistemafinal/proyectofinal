angular.module('app').controller('CategoriasCtrl', function (toastr, CONEX, $mdDialog) {

    var self = this;

    self.listar = function(search){
      var filtro = {
        search: search,
      };
      CONEX('ventas','categorias').GET_LIST(filtro).$promise.then(function (r) {
            self.listado = r;
        }, function (err) {
        });
    };

    self.listar();

    self.new_edit = function(ev, obj) {
    $mdDialog.show({
      controller: 'CategoriasFormCtrl',
      controllerAs: "form",
      templateUrl: 'views/categorias/form.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:false,
      fullscreen: false, // only for -xs, -sm beakpoints
      locals: {obj: obj},
    })
    .then(function(obj) {
      self.listar();
    }, function() {
      
    });
  };

self.delete = function(ev, obj) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('¿Desea eliminar la Categoria: '+obj['nombre']+'?')
          .textContent('Mira, si eliminas ya no podrás recuperar esta Categoria')
          .ariaLabel('a')
          .targetEvent(ev)
          .ok('Si, estoy seguro')
          .cancel('No quiero cho');

    $mdDialog.show(confirm).then(function() {
      CONEX('ventas','categorias/delete').DELETE({id: obj['id']}).$promise.then(function (r){
          self.listar();
        }, function(err){
          toastr.error('No es Posible eliminar esta categoria, seguramente Esta anexado a un Producto');
        });
    }, function() {
      toastr.info('Ok, no eliminaste');
    });
  };
});

angular.module('app').controller('CategoriasFormCtrl', function (toastr, CONEX, $mdDialog, obj) {
    var self = this;
    self.obj = {};
    //self.tiposgrupo = [];

    if(obj){
       CONEX('ventas','categorias/edit').GET({id: obj['id']}).$promise.then(function (r){
          self.obj = r;
        }, function(err){

        });
    }

    self.cancel = function(){
        $mdDialog.cancel();
    };    

    self.guardar = function(search){
      if(self.obj['id']){
      CONEX('ventas','categorias/update').PUT(self.obj).$promise.then(function (r) {
            toastr.success("Ctegoria actualizado correctamente");
            $mdDialog.hide(r);
        }, function (err) {
          toastr.error(err.data, "Error al actualizar")
        });
      }else{
      self.obj['estado'] = '1';
      CONEX('ventas','categorias/add').POST(self.obj).$promise.then(function (r) {
            toastr.success("categorias creada correctamente");
            $mdDialog.hide(r);
        }, function (err) {
          toastr.error(err.data, "Error al registrar")
        }); 
    }
    };

});