from django.conf.urls import url, include
from rest_framework import routers
from .apis.PersonaApi import PersonasViewSet
from .apis.CategoriaApi import CategoriasViewSet
from .apis.ProductoApi import ProductosViewSet
from .apis.RolApi import RolesViewSet
from .apis.UserApi import UsersViewSet
from .apis.ProveedorApi import ProveedoresViewSet
from .apis.DetalleIngresoApi import DetalleingresosViewSet
from .apis.DetalleVentaApi import DetalleVentaViewSet
from .apis.IngresosApi import IngresosViewSet
from .apis.VentaApi import VentaViewSet
from .CrudSoap import Producto
from .CrudSoap import Persona
#from .views import Soapdetalleventa

router = routers.DefaultRouter()
router.register(r'personas', PersonasViewSet)
router.register(r'categorias', CategoriasViewSet)
router.register(r'producto', ProductosViewSet)
router.register(r'roles', RolesViewSet)
router.register(r'users', UsersViewSet)
router.register(r'prove', ProveedoresViewSet)
router.register(r'detain', DetalleingresosViewSet)
router.register(r'detave', DetalleVentaViewSet)
router.register(r'ingresos', IngresosViewSet)
router.register(r'ventas', VentaViewSet)

urlpatterns = [
    #path('admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url(r'^prod/', Producto.my_soap_consulta()),
    url(r'^pers/', Persona.my_soap_consulta()),
   
]