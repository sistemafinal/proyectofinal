from django.db import models
from ..models.Ingresos import Ingresos
from ..models.Productos import Productos

class Detalle_ingreso(models.Model):
	ingreso = models.ForeignKey(Ingresos, related_name='ingresos',
								   db_column='ingresos_id', on_delete=models.PROTECT)
	productos = models.ForeignKey(Productos, related_name='productos',
								   db_column='producto_id', on_delete=models.PROTECT)
	cantidad = models.IntegerField()
	precio = models.DecimalField(max_digits=12, decimal_places=2)
	
class Meta:
	verbose_name = 'DetalleIngreso'
	verbose_name_plural = 'DetalleIngresos'
	default_permissions = ()

def __str__(self):
	return self.nombre
