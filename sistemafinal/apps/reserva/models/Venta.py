from django.db import models
from ..models.Personas import Personas
from ..models.Users import Users


class Venta(models.Model):
    codigo = models.CharField(max_length=15, unique=True,)
    serie_comprobante = models.CharField(max_length=50,)
    num_comprobante = models.CharField(max_length=50,)
    fecha_hora = models.DateField(blank=True, null=True,)
    impuesto = models.DecimalField(max_digits=12, decimal_places=2,)
    total = models.DecimalField(max_digits=12, decimal_places=2,)
    cliente = models.ForeignKey(Personas, related_name='cliente_venta', db_column='cliente_id', on_delete=models.PROTECT, blank=True, null=True,)
    user = models.ForeignKey(Users, related_name='users_venta',
                                 db_column='user_id', on_delete=models.PROTECT, blank=True, null=True,)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class  Meta:
	verbose_name = 'venta'
	verbose_name_plural = 'ventas'
	default_permissions = ()
			
def __str__(self):
 return self.nombre