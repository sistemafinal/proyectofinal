
app.config(function ($mdThemingProvider) {

    $mdThemingProvider.theme('Blue')
            .primaryPalette('blue');

    $mdThemingProvider.theme('Teal')
            .primaryPalette('teal');

    $mdThemingProvider.theme('Orange')
            .primaryPalette('orange');

    $mdThemingProvider.setDefaultTheme('Blue');
    $mdThemingProvider.alwaysWatchTheme(true);
});
