from django.conf.urls import url, include
from ..models.Users import Users
from rest_framework import routers, serializers, viewsets
from django.db.models.deletion import ProtectedError
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.reserva.always.SearchFilter import keys_add_none
from rest_framework import permissions

class UsersSerializer(serializers.ModelSerializer):
    roles_nombre = serializers.CharField(source='roles.nombre')
    class Meta:
        model = Users
        fields = '__all__'

class UsersBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('id','usuario','password')

class UsersinfoSerializer(serializers.ModelSerializer):
    roles_nombre = serializers.CharField(source='roles.nombre')
    class Meta:
        model = Users
        fields = ('id', 'usuario', 'password', 'estado','roles_nombre',)

class UsersViewSet(viewsets.ModelViewSet):
    queryset = Users.objects.all()
    serializer_class = UsersSerializer

    @list_route(url_path='searchform', methods=['get'], permission_classes=[])
    def get_searchform(self, request, *args, **kwargs):
        listado = Users.objects.filter().order_by('usuario')
        result = UsersBasicSerializer(listado, many=True).data
        return Response(result)

    @list_route(url_path='add', methods=['post'], permission_classes=[])
    def get_add(self, request, *args, **kwargs):
        data = request.data
        data_ = keys_add_none(data,'usuario,password,estado,rol_id')
        e = Users.objects.create(**data_)
        return Response(self.get_serializer(e).data)

    @list_route(url_path='edit', methods=['get'], permission_classes=[])
    def get_edit(self, request, *args, **kwargs):
        id = request.query_params.get('id', None)
        g = Users.objects.filter(id=id).values().first()
        return Response(g)

    @list_route(url_path='update', methods=['put'], permission_classes=[])
    def get_update(self, request, *args, **kwargs):
        data = request.data
        columns = 'usuario,password,estado,rol_id'
        data_ = keys_add_none(data, columns)
        g = Users.objects.filter(id=data['id']).update(**data)
        if g:
            model = Users.objects.get(id=data['id'])
            result = self.get_serializer(model).data
            return Response('no hemos podido Actualizar')
        else:
            return Response('NO HEMOS PODIDO ACTUALIZAR')

    @list_route(url_path='delete', methods=['delete'], permission_classes=[])
    def get_delete(self, request, *args, **kwargs):
        id = request.query_params.get('id', None)
        try:
            g = Users.objects.filter(id=id).delete()
            return Response({'id': id, 'msg': 'se elimino correctamente'})
        except ProtectedError as e:
            return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)
